//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by notepad.rc

#define IDS_APP_TITLE			103

#define IDR_MAINFRAME			128
#define IDD_NOTEPAD_DIALOG	102
#define IDD_ABOUTBOX			103
#define IDM_ABOUT				104
#define IDM_EXIT				105
#define IDM_NEW_TAB				106
#define IDM_NEW_WINDOW			107
#define IDM_OPEN				108
#define IDM_SAVE				109
#define IDM_SAVE_AS				110
#define IDM_SAVE_ALL			111
#define IDM_PAGE_SETTINGS		112
#define IDM_PRINT				113
#define	IDM_CLOSE_TAB			114
#define IDM_CLOSE_WINDOW		115

#define IDM_UNDO				120
#define IDM_CUT					121
#define IDM_COPY				122
#define IDM_PASTE				123
#define IDM_DELETE				124
#define IDM_FIND				125
#define	IDM_FIND_NEXT			126
#define IDM_FIND_PREVIOUS		127
#define IDM_REPLACE				128
#define IDM_JUMP_TO				129
#define IDM_SELECT_ALL			130
#define IDM_HOUR_DAY			131
#define IDM_FONT				132

#define IDM_MAGNIFICATION		140
#define IDM_ZOOM_IN				141
#define IDM_ZOOM_OUT			142
#define IDM_DEFAULT_ZOOM		143
#define IDM_STATUS_BAR			144
#define IDM_ROW_WRAP			145

#define IDI_NOTEPAD			200
#define IDI_SMALL				201
#define IDC_NOTEPAD			300
#define IDC_MYICON				2
#ifndef IDC_STATIC
#define IDC_STATIC				-1
#endif
// Next default values for new objects
//
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS

#define _APS_NO_MFC					130
#define _APS_NEXT_RESOURCE_VALUE	129
#define _APS_NEXT_COMMAND_VALUE		32771
#define _APS_NEXT_CONTROL_VALUE		1000
#define _APS_NEXT_SYMED_VALUE		110
#endif
#endif

