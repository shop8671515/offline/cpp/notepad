#pragma once

#ifndef _NOTEPAD_
#define _NOTEPAD_

#include <Windows.h>
#include <CommCtrl.h>
#include <cstring>
#include <cwchar>
#include <string>
#include <vector>
#include <algorithm>

#include "resource.h"

#pragma comment(lib, "comctl32.lib")

#define MAX_LOADSTRING 100
#define NOTEPAD_BEGIN namespace NOTEPAD {
#define NOTEPAD_END }

const WCHAR szTitle[MAX_LOADSTRING] = L"Notepad";
const WCHAR szWindowClass[MAX_LOADSTRING] = L"Notepad";

NOTEPAD_BEGIN

struct MENU_BUTTON_INFO
{
	_In_ HMENU hMenu;
	_In_ UINT uFlags;
	_In_ UINT_PTR uIDNewItem;
	_In_opt_ LPCWSTR lpNewItem;
};

HWND hEdit;

HMENU hFileMenu = CreateMenu();
HMENU hEditMenu = CreateMenu();
HMENU hDisplayMenu = CreateMenu();

HMENU hDisplayPopMenu = CreatePopupMenu();

std::vector<MENU_BUTTON_INFO> menuFileButtons =
{
	{ hFileMenu, MF_STRING, IDM_NEW_TAB, L"New tab\tCtrl+N" },
	{ hFileMenu, MF_STRING, IDM_NEW_WINDOW, L"New Window\tCtrl+Shift+N" },
	{ hFileMenu, MF_STRING, IDM_OPEN, L"Open\tCtrl+O" },
	{ hFileMenu, MF_STRING, IDM_SAVE, L"Save\tCtrl+S" },
	{ hFileMenu, MF_STRING, IDM_SAVE_AS, L"Save as\tCtrl+Shift+S" },
	{ hFileMenu, MF_STRING, IDM_SAVE_ALL, L"Save all\tCtrl+Alt+S" },
	{ hFileMenu, MF_SEPARATOR, 0, nullptr },
	{ hFileMenu, MF_STRING, IDM_PAGE_SETTINGS, L"Page settings" },
	{ hFileMenu, MF_STRING, IDM_PRINT, L"Print\tCtrl+P" },
	{ hFileMenu, MF_SEPARATOR, 0, nullptr },
	{ hFileMenu, MF_STRING, IDM_CLOSE_TAB, L"Close tab\tCtrl+W" },
	{ hFileMenu, MF_STRING, IDM_CLOSE_WINDOW, L"Close Window\tCtrl+Shift+W" },
	{ hFileMenu, MF_STRING, IDM_EXIT, L"Exit" }
};

std::vector<MENU_BUTTON_INFO> menuEditButtons =
{
	{ hEditMenu, MF_STRING, IDM_UNDO, L"Undo\tCtrl+Z" },
	{ hEditMenu, MF_SEPARATOR, 0, nullptr },
	{ hEditMenu, MF_STRING, IDM_CUT, L"Cut\tCtrl+X" },
	{ hEditMenu, MF_STRING, IDM_COPY, L"Copy\tCtrl+C" },
	{ hEditMenu, MF_STRING, IDM_PASTE, L"Paste\tCtrl+V" },
	{ hEditMenu, MF_STRING, IDM_DELETE, L"Delete\tDel" },
	{ hEditMenu, MF_SEPARATOR, 0, nullptr },
	{ hEditMenu, MF_STRING, IDM_FIND, L"Find\tCtrl+F" },
	{ hEditMenu, MF_STRING, IDM_FIND_NEXT, L"Find next\tF3" },
	{ hEditMenu, MF_STRING, IDM_FIND_PREVIOUS, L"Find previous\tShift+F3" },
	{ hEditMenu, MF_STRING, IDM_REPLACE, L"Replace\tCtrl+H" },
	{ hEditMenu, MF_STRING, IDM_JUMP_TO, L"Jump to\tCtrl+G" },
	{ hEditMenu, MF_SEPARATOR, 0, nullptr },
	{ hEditMenu, MF_STRING, IDM_SELECT_ALL, L"Select all\tCtrl+A" },
	{ hEditMenu, MF_STRING, IDM_HOUR_DAY, L"Hour/Day\tF5" },
	{ hEditMenu, MF_SEPARATOR, 0, nullptr },
	{ hEditMenu, MF_STRING, IDM_FONT, L"Font" }
};

std::vector<MENU_BUTTON_INFO> menuDisplayButtons =
{
	{ hDisplayMenu, MF_STRING | MF_POPUP, (UINT_PTR) hDisplayPopMenu, L"Magnification" },
	{ hDisplayPopMenu, MF_STRING, IDM_ZOOM_IN, L"Zoom in\tCtrl+Plus"},
	{ hDisplayPopMenu, MF_STRING, IDM_ZOOM_OUT, L"Zoom out\tCtrl+Minus"},
	{ hDisplayPopMenu, MF_STRING, IDM_DEFAULT_ZOOM, L"Default zoom\tCtrl+0"},
	{ hDisplayMenu, MF_STRING, IDM_STATUS_BAR, L"Status bar" },
	{ hDisplayMenu, MF_STRING, IDM_ROW_WRAP, L"Row wrapping" }
};

VOID ShowSideMenu(HWND hwnd) 
{
	POINT cursorPos;
	GetCursorPos(&cursorPos);

	// TrackPopupMenu displays the menu at the specified location
	TrackPopupMenu(hDisplayPopMenu, TPM_RIGHTALIGN | TPM_TOPALIGN, cursorPos.x, cursorPos.y, 0, hwnd, nullptr);
}

void UpdateScrollBarVisibility(HWND hwnd) {
	// Get the horizontal and vertical scroll bar visibility
	BOOL bHorzVisible = GetWindowLong(hEdit, GWL_STYLE) & WS_HSCROLL;
	BOOL bVertVisible = GetWindowLong(hEdit, GWL_STYLE) & WS_VSCROLL;

	// Show or hide the horizontal scroll bar based on content width
	SCROLLINFO si;
	si.cbSize = sizeof(SCROLLINFO);
	si.fMask = SIF_ALL;
	GetScrollInfo(hEdit, SB_HORZ, &si);
	ShowScrollBar(hEdit, SB_HORZ, si.nMax > si.nPage);

	// Show or hide the vertical scroll bar based on content height
	GetScrollInfo(hEdit, SB_VERT, &si);
	ShowScrollBar(hEdit, SB_VERT, si.nMax > si.nPage);
}

void _Assert_true(bool statement, const std::exception& ex)
{
	if (!statement)
	{
		throw ex;
	}
}

VOID ToggleCheckmark(UINT menuItemID) 
{
	MENUITEMINFO menuItemInfo = { sizeof(MENUITEMINFO) };
	menuItemInfo.fMask = MIIM_STATE;

	// Get the current state of the menu item
	GetMenuItemInfo(hDisplayMenu, menuItemID, FALSE, &menuItemInfo);

	// Toggle the checked state
	menuItemInfo.fState ^= MFS_CHECKED;

	// Set the updated state
	SetMenuItemInfo(hDisplayMenu, menuItemID, FALSE, &menuItemInfo);
}

VOID CreateMenuDropdown(HMENU* hMenu, LPCWSTR menuName, const std::vector<MENU_BUTTON_INFO>& menuButtons)
{
	std::for_each(menuButtons.begin(), menuButtons.end(), [](const MENU_BUTTON_INFO& menuButtonInfo)
		{
			AppendMenu(menuButtonInfo.hMenu, menuButtonInfo.uFlags, menuButtonInfo.uIDNewItem, menuButtonInfo.lpNewItem);
		});

	AppendMenu(*hMenu, MF_POPUP, (UINT_PTR)menuButtons[0].hMenu, menuName);
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);

		switch (wmId)
		{
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		case IDM_STATUS_BAR:
			ToggleCheckmark(IDM_STATUS_BAR);
			break;
		case IDM_ROW_WRAP:
			ToggleCheckmark(IDM_ROW_WRAP);
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
	case WM_SIZE:
            // Adjust the size of the EDIT control when the window is resized
            MoveWindow(hEdit, 0, 0, LOWORD(lParam), HIWORD(lParam), TRUE);
            break;
	break;
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code that uses hdc here...
		EndPaint(hWnd, &ps);
	}
	break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

ATOM RegClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_NOTEPAD));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_NOTEPAD);
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassExW(&wcex);
}

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

	if (!hWnd)
	{
		return FALSE;
	}

	RECT clientRect;
	GetClientRect(hWnd, &clientRect);

	hEdit = CreateWindowExW(
		WS_EX_CLIENTEDGE,   // Extended window style
		L"EDIT",            // Window class name
		nullptr,            // Window text (empty for now)
		WS_CHILD | WS_VISIBLE | WS_VSCROLL | WS_HSCROLL | ES_MULTILINE | ES_AUTOVSCROLL | ES_AUTOHSCROLL,
		0,                  // X-coordinate
		0,                  // Y-coordinate
		clientRect.right - clientRect.left,  // Width
		clientRect.bottom - clientRect.top,  // Height
		hWnd,               // Parent window handle
		nullptr,            // Menu handle
		nullptr,            // Instance handle
		nullptr             // Additional application data
	);

	HMENU hMenu = CreateMenu();
	CreateMenuDropdown(&hMenu, L"File", menuFileButtons);
	CreateMenuDropdown(&hMenu, L"Edit", menuEditButtons);
	CreateMenuDropdown(&hMenu, L"Display", menuDisplayButtons);

	SetMenu(hWnd, hMenu);

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return TRUE;
}

NOTEPAD_END


#endif
